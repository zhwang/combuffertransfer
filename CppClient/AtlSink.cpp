// AtlSink.cpp : Implementation of CAtlSink

#include "stdafx.h"
#include "AtlSink.h"


// CAtlSink

HRESULT CAtlSink::FinalConstruct()
{
	//CoInitialize(NULL);
	auto hr = m_my_admin.CoCreateInstance(CLSID_MyAdmin);
	if (SUCCEEDED(hr))
	{
	}
	else
	{
		::MessageBox(::GetDesktopWindow(), _T("Create myadmin failed!"), _T("CppClient Final Construct"), MB_ICONINFORMATION);
	}

	//Subscribe the image events

	if (!m_splicecam_events_cookie)
	{
		hr = m_my_admin.Advise(GetUnknown(), __uuidof(_IPseudoCameraEvents), &m_splicecam_events_cookie);
		if (FAILED(hr))
		{
			::MessageBox(::GetDesktopWindow(), _T("Advise Camera event failed!"), _T("CppClient Final Construct"), MB_ICONINFORMATION);
			return hr;
		}
	}


	return S_OK;
}

void CAtlSink::FinalRelease()
{
	HRESULT hr;

	CComQIPtr<IConnectionPointContainer> cpc = m_my_admin;
	CComPtr<IConnectionPoint> cp;

	if (m_splicecam_events_cookie)
	{
		hr = cpc->FindConnectionPoint(__uuidof(_IPseudoCameraEvents), &cp);
		if (SUCCEEDED(hr))                              // Is the advise alive?
		{
			hr = cp->Unadvise(m_splicecam_events_cookie);// Stop listening.
			cp.Release();                               // Finished with connection point.
		}
		m_splicecam_events_cookie = 0;                  // Reset this dead cookie.
	}
	cpc.Release();

	//CoUninitialize();
}

//
STDMETHODIMP CAtlSink::NewSpliceImage(FasCamBITMAPINFO* image)
{
	auto x = image->bmiHeader.biWidth;
	auto y = image->bmiHeader.biHeight;

	//::MessageBox(::GetDesktopWindow(), _T("calling !"), _T("CppClinetDlg"), MB_ICONINFORMATION);

	return S_OK;
}