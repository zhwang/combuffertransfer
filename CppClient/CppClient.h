
// CppClient.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "CppClient_i.h"


// CCppClientApp:
// See CppClient.cpp for the implementation of this class
//

class CCppClientApp : public CWinApp
{
public:
	CCppClientApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	BOOL ExitInstance();
};

extern CCppClientApp theApp;
