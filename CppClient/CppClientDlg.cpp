
// CppClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CppClient.h"
#include "CppClientDlg.h"
#include "afxdialogex.h"
#include "AtlSink.h"


//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CCppClientDlg dialog



CCppClientDlg::CCppClientDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CPPCLIENT_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCppClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCppClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CCppClientDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// CCppClientDlg message handlers

BOOL CCppClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	// TODO: Add extra initialization here
	sink = new Sink;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCppClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCppClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCppClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//void CCppClientDlg::OnBnClickedButton2()
//{
//	auto hr = m_atlSink.CoCreateInstance(__uuidof(AtlSink));
//
//	if (FAILED(hr))
//	{
//		::MessageBox(::GetDesktopWindow(), _T("Advise camera events error!"), _T("CppClinetDlg"), MB_ICONINFORMATION);
//		return;
//	}
//
//}


void CCppClientDlg::OnBnClickedButton2()
{
	CoInitialize(nullptr);

	auto hr = m_myAdmin.CoCreateInstance(__uuidof(MyAdmin));
	if (FAILED(hr))
	{
		::MessageBox(::GetDesktopWindow(), _T("create my admin error!"), _T("CppClinetDlg"), MB_ICONINFORMATION);
		return;
	}

	CComQIPtr<IConnectionPointContainer> pcn = m_myAdmin;
	if (!pcn) return;

	hr = pcn->FindConnectionPoint(__uuidof(_IPseudoCameraEvents), &pCP);
	if (FAILED(hr)) return;

	IUnknown* pSinkUnk;

	hr = sink->QueryInterface(IID_IUnknown, (void **)(&pSinkUnk));
	if (FAILED(hr)) return;


	// The return value is S_OK but call back function is not triggered.
	hr = pCP->Advise(pSinkUnk, &m_splicecam_events_cookie);

	//hr = m_myAdmin.Advise(pSinkUnk, __uuidof(_IPseudoCameraEvents), &m_splicecam_events_cookie);

	if (FAILED(hr))
	{
		::MessageBox(::GetDesktopWindow(), _T("Advise camera events error!"), _T("CppClinetDlg"), MB_ICONINFORMATION);
		return;
	}

}
