#pragma once


#include "CallWorkerExe_i.h";
#import "..\CallWorkerExe\x64\Debug\CallWorkerExe.tlb" named_guids raw_interfaces_only 

using namespace ATL;

class Sink : public _IPseudoCameraEvents
{

private:
	DWORD   m_dwRefCount;

public:
	Sink();
	virtual ~Sink();

	HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void **ppvObject)
	{
		if (iid == IID__IPseudoCameraEvents)
		{
			m_dwRefCount++;
			*ppvObject = static_cast<_IPseudoCameraEvents*>(this);
			return S_OK;
		}

		if (iid == IID_IUnknown)
		{
			m_dwRefCount++;
			*ppvObject = static_cast<_IPseudoCameraEvents*>(this);
			return S_OK;
		}

		return E_NOINTERFACE;
	}
	ULONG STDMETHODCALLTYPE AddRef()
	{
		m_dwRefCount++;
		return m_dwRefCount;
	}

	ULONG STDMETHODCALLTYPE Release()
	{
		ULONG l;

		l = m_dwRefCount--;

		if (0 == m_dwRefCount)
		{
			delete this;
		}

		return l;
	}
	

	HRESULT __stdcall NewSpliceImage(FasCamBITMAPINFO* image);


};
