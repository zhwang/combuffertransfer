
CppClientps.dll: dlldata.obj CppClient_p.obj CppClient_i.obj
	link /dll /out:CppClientps.dll /def:CppClientps.def /entry:DllMain dlldata.obj CppClient_p.obj CppClient_i.obj \
		kernel32.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \
.c.obj:
	cl /c /Ox /DREGISTER_PROXY_DLL \
		$<

clean:
	@del CppClientps.dll
	@del CppClientps.lib
	@del CppClientps.exp
	@del dlldata.obj
	@del CppClient_p.obj
	@del CppClient_i.obj
