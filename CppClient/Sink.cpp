#include "stdafx.h"
#include "Sink.h"


Sink::Sink()
{
	m_dwRefCount = 0;
}


Sink::~Sink()
{
}


HRESULT __stdcall Sink::NewSpliceImage(FasCamBITMAPINFO* image)
{
	auto x = image->bmiHeader.biWidth;
	auto y = image->bmiHeader.biHeight;

	//::MessageBox(::GetDesktopWindow(), _T("calling !"), _T("CppClinetDlg"), MB_ICONINFORMATION);
	ATLTRACE(_T("CppClinetDlg::Sink::NewSpliceImage called {0x%X,0x%X,0x%X,0x%X,0x%X,0x%X,0x%X,0x%X}\n"), image->bmiBits[0],
																											image->bmiBits[1], 
																											image->bmiBits[2], 
																											image->bmiBits[3], 
																											image->bmiBits[4], 
																											image->bmiBits[5], 
																											image->bmiBits[6], 
																											image->bmiBits[7]);

	return S_OK;
}