// CppClientDlg.h : header file
//
#pragma once
#include "CallWorkerExe_i.h";
#import "..\CallWorkerExe\x64\Debug\CallWorkerExe.tlb" named_guids raw_interfaces_only 

#include "Sink.h"

using namespace ATL;

// CCppClientDlg dialog
class CCppClientDlg : public CDialogEx
{
// Construction
public:

	CCppClientDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CPPCLIENT_DIALOG };
#endif


	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	Sink* sink;
	CComQIPtr<_IPseudoCameraEvents> m_atlSink;

// Implementation
protected:
	HICON m_hIcon;
	CComQIPtr<_IPseudoCameraEvents> m_myAdmin = nullptr;
	unsigned long m_splicecam_events_cookie = 0;                        // Camera connection point advise cookie.

	LONG m_dwRefCount = 0;
	CComQIPtr<IConnectionPoint> pCP;
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
};
