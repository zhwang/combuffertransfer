// AtlSink.h : Declaration of the CAtlSink

#pragma once
#include "resource.h"       // main symbols

#include "CppClient_i.h"
#include "CallWorkerExe_i.h";
#include "_IPseudoCameraEvents_CP.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CAtlSink

class ATL_NO_VTABLE CAtlSink :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CAtlSink, &CLSID_AtlSink>,
	public IConnectionPointContainerImpl<CAtlSink>,
	public CProxy_IPseudoCameraEvents<CAtlSink>,
	public IDispatchImpl<IAtlSink, &IID_IAtlSink, &LIBID_CppClientLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public _IPseudoCameraEvents
{
public:
	CAtlSink()
	{
		m_splicecam_events_cookie = 0;
	}

DECLARE_REGISTRY_RESOURCEID(104)


BEGIN_COM_MAP(CAtlSink)
	COM_INTERFACE_ENTRY(IAtlSink)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(_IPseudoCameraEvents)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CAtlSink)
	CONNECTION_POINT_ENTRY(__uuidof(_IPseudoCameraEvents))
END_CONNECTION_POINT_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();

	void FinalRelease();


	unsigned long m_splicecam_events_cookie;                        // Camera connection point advise cookie.
	CComQIPtr<IMyAdmin> m_my_admin;

	//HRESULT NewSpliceImage(FasCamBITMAPINFO *);      // [in]bmp_info

	STDMETHOD(NewSpliceImage)(FasCamBITMAPINFO * img);      // [in]bmp_info
};

OBJECT_ENTRY_AUTO(__uuidof(AtlSink), CAtlSink)
