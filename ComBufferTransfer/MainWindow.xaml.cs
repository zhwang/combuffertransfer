﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using CallWorkerExeLib;

namespace ComBufferTransfer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MyAdminClass _myAdmin;
        public MainWindow()
        {
            InitializeComponent();

            _myAdmin = new MyAdminClass();
            //var myadmin = _myAdmin as MyAdmin;
            _myAdmin.NewSpliceImage += MyadminOnNewSpliceImage;

        }

        private void MyadminOnNewSpliceImage(ref FasCamBITMAPINFO bmp_info)
        {

            var imageData = new byte[bmp_info.bmiHeader.biHeight*bmp_info.bmiHeader.biWidth];
            // Access violation may happen sometimes if copy all the pixels from source buffer.
            // Just copy 2 bytes for example.
            Marshal.Copy(bmp_info.bmiBits, imageData, 0, 2);
            Debug.WriteLine($"First Byte of Image data = {imageData[0]:X2}; Second Byte of Image data = {imageData[1]:X2}");

        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            if (_myAdmin != null)
            {
                _myAdmin.NewSpliceImage -= MyadminOnNewSpliceImage;
            }

        }
    }
}
