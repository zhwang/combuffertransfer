

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Tue Jan 19 11:14:07 2038
 */
/* Compiler settings for ..\Common\BitmapInfo.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.01.0622 
    protocol : all , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */



/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */


#ifndef __BitmapInfo_h__
#define __BitmapInfo_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_BitmapInfo_0000_0000 */
/* [local] */ 


enum FasCamFileType
    {
        FASCAMFILE_DIB	= 0,
        FASCAMFILE_PNG	= ( FASCAMFILE_DIB + 1 ) ,
        FASCAMFILE_JPG	= ( FASCAMFILE_PNG + 1 ) 
    } ;

enum FasCamOrientationType
    {
        TOP_DOWN	= 0,
        TOP_DOWN_ROT_POS	= ( TOP_DOWN + 1 ) ,
        TOP_DOWN_FLIPPED	= ( TOP_DOWN_ROT_POS + 1 ) ,
        TOP_DOWN_ROT_NEG	= ( TOP_DOWN_FLIPPED + 1 ) ,
        BOTTOM_UP	= ( TOP_DOWN_ROT_NEG + 1 ) ,
        BOTTOM_UP_ROT_POS	= ( BOTTOM_UP + 1 ) ,
        BOTTOM_UP_FLIPPED	= ( BOTTOM_UP_ROT_POS + 1 ) ,
        BOTTOM_UP_ROT_NEG	= ( BOTTOM_UP_FLIPPED + 1 ) 
    } ;
typedef /* [public] */ struct __MIDL___MIDL_itf_BitmapInfo_0000_0000_0001
    {
    struct _BITMAPINFOHEADER1
        {
        DWORD biSize;
        LONG biWidth;
        LONG biHeight;
        WORD biPlanes;
        WORD biBitCount;
        DWORD biCompression;
        DWORD biSizeImage;
        LONG biXPelsPerMeter;
        LONG biYPelsPerMeter;
        DWORD biClrUsed;
        DWORD biClrImportant;
        } 	bmiHeader;
    struct _RGBQUAD1
        {
        BYTE rgbBlue;
        BYTE rgbGreen;
        BYTE rgbRed;
        BYTE rgbReserved;
        } 	bmiColors[ 256 ];
    DOUBLE bmiYMicronsPerPixel;
    DOUBLE bmiZMicronsPerPixel;
    ULONGLONG bmiTimeStamp;
    int bmiCoSize;
    /* [size_is] */ BYTE *bmiBits;
    } 	FasCamBITMAPINFO;

typedef /* [public] */ struct __MIDL___MIDL_itf_BitmapInfo_0000_0000_0002
    {
    struct _BITMAPINFOHEADER
        {
        DWORD biSize;
        LONG biWidth;
        LONG biHeight;
        WORD biPlanes;
        WORD biBitCount;
        DWORD biCompression;
        DWORD biSizeImage;
        LONG biXPelsPerMeter;
        LONG biYPelsPerMeter;
        DWORD biClrUsed;
        DWORD biClrImportant;
        } 	bmiHeader;
    struct _RGBQUAD
        {
        BYTE rgbBlue;
        BYTE rgbGreen;
        BYTE rgbRed;
        BYTE rgbReserved;
        } 	bmiColors[ 256 ];
    DOUBLE bmiYMicronsPerPixel;
    DOUBLE bmiZMicronsPerPixel;
    ULONGLONG bmiTimeStamp;
    SAFEARRAY bmiBits;
    } 	FasCamBITMAPINFO_Array;



extern RPC_IF_HANDLE __MIDL_itf_BitmapInfo_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_BitmapInfo_0000_0000_v0_0_s_ifspec;

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


