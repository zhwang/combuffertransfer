// CallWorkerExe.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "CallWorkerExe_i.h"


using namespace ATL;


class CCallWorkerExeModule : public ATL::CAtlExeModuleT< CCallWorkerExeModule >
{
public :
	DECLARE_LIBID(LIBID_CallWorkerExeLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CALLWORKEREXE, "{5089698e-1d60-4b00-a155-2394dec261b7}")
};

CCallWorkerExeModule _AtlModule;




//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/,
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	//CCallWorkerExeModule::InitializeCom();

	return _AtlModule.WinMain(nShowCmd);
}

