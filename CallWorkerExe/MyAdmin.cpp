#include "stdafx.h"
#include "MyAdmin.h"
#include "WorkerComponent.h"

HRESULT CMyAdmin::FinalConstruct()
{
	//CoInitialize(NULL);
	::MessageBox(::GetDesktopWindow(), _T("Pause"), _T("MyAdmin Final Construct"), MB_ICONINFORMATION);

	auto hr = _pPseudoCamera.CoCreateInstance(CLSID_PseudoCamera);
	if(SUCCEEDED(hr))
	{
		//::MessageBox(::GetDesktopWindow(), _T("Create Camera success!"), _T("MyAdmin Final Construct"), MB_ICONINFORMATION);
	}
	else
	{
		::MessageBox(::GetDesktopWindow(), _T("Create Camera failed!"), _T("MyAdmin Final Construct"), MB_ICONINFORMATION);
	}

	//Subscribe the image events

	if (!m_splicecam_events_cookie)
	{
		hr = _pPseudoCamera.Advise(GetUnknown(), __uuidof(_IPseudoCameraEvents), &m_splicecam_events_cookie);
		if (FAILED(hr))
		{
			::MessageBox(::GetDesktopWindow(), _T("Advise Camera event failed!"), _T("MyAdmin Final Construct"), MB_ICONINFORMATION);
			return hr;
		}
	}


	return S_OK;
}

void CMyAdmin::FinalRelease()
{
	HRESULT hr;

	CComQIPtr<IConnectionPointContainer> cpc = _pPseudoCamera;
	CComPtr<IConnectionPoint> cp;

	if (m_splicecam_events_cookie)
	{
		hr = cpc->FindConnectionPoint(__uuidof(_IPseudoCameraEvents), &cp);
		if (SUCCEEDED(hr))                              // Is the advise alive?
		{
			hr = cp->Unadvise(m_splicecam_events_cookie);// Stop listening.
			cp.Release();                               // Finished with connection point.
		}
		m_splicecam_events_cookie = 0;                  // Reset this dead cookie.
	}
	cpc.Release();

	//CoUninitialize();
}


STDMETHODIMP CMyAdmin::Initialize(LONG nProduce)
{
	return S_OK;
}

STDMETHODIMP CMyAdmin::NewSpliceImage(FasCamBITMAPINFO* image)
{
	Fire_NewSpliceImage(image);
	return S_OK;
}

