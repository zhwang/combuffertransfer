// MyAdmin.h : Declaration of the CMyAdmin

#pragma once
#include "Resource.h"       // main symbols

#include "CallWorkerExe_i.h"
#include "_IPseudoCameraEvents_CP.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;

// CMyAdmin

class ATL_NO_VTABLE CMyAdmin :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CMyAdmin, &CLSID_MyAdmin>,
	public IConnectionPointContainerImpl<CMyAdmin>,
	public CProxy_IPseudoCameraEvents<CMyAdmin>,
	public IDispatchImpl<IMyAdmin, &IID_IMyAdmin, &LIBID_CallWorkerExeLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public _IPseudoCameraEvents
{
private:
	CComQIPtr<IPseudoCamera> _pPseudoCamera;
	unsigned long m_splicecam_events_cookie = 0;                        // Camera connection point advise cookie.

public:
	CMyAdmin() {}

	DECLARE_REGISTRY_RESOURCEID(IDR_MYADMIN)


	BEGIN_COM_MAP(CMyAdmin)
		COM_INTERFACE_ENTRY(IMyAdmin)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IConnectionPointContainer)
		COM_INTERFACE_ENTRY(_IPseudoCameraEvents)
	END_COM_MAP()

	BEGIN_CONNECTION_POINT_MAP(CMyAdmin)
		CONNECTION_POINT_ENTRY(__uuidof(_IPseudoCameraEvents))
	END_CONNECTION_POINT_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();

	void FinalRelease();

	STDMETHOD(Initialize)(LONG nProduce);

	// _IPseudoCameraEvents
		// _IPseudoCameraEvents
	STDMETHOD(NewSpliceImage)(FasCamBITMAPINFO *);      // [in]bmp_info


};

OBJECT_ENTRY_AUTO(__uuidof(MyAdmin), CMyAdmin)
