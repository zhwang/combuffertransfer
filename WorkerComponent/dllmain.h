// dllmain.h : Declaration of module class.

class CWorkerComponentModule : public ATL::CAtlDllModuleT< CWorkerComponentModule >
{
public :
	DECLARE_LIBID(LIBID_WorkerComponentLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_WORKERCOMPONENT, "{2d21feda-f447-4140-84da-e762ac189285}")
};

extern class CWorkerComponentModule _AtlModule;
