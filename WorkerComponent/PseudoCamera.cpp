// PseudoCamera.cpp : Implementation of CPseudoCamera

#include "stdafx.h"
#include "PseudoCamera.h"

#ifndef FASCAM_THREAD_MODEL
#define FASCAM_THREAD_MODEL		        COINIT_APARTMENTTHREADED
#endif

#ifndef IMAGE_PALETTE_SIZE
#define IMAGE_PALETTE_SIZE  256
#endif


// CPseudoCamera

HRESULT CPseudoCamera::FinalConstruct()
{
	// EDIT ----------------------------------------------
	m_pStream = NULL;																				
	IUnknown *punk = GetUnknown();																	
	
	if (punk)																						
	{
		::CoMarshalInterThreadInterfaceInStream(__uuidof(_IPseudoCameraEvents), punk, &m_pStream);	
		punk->Release();																						
	}
	// EDIT end

	m_cam_thread_mutex = ::CreateMutex(nullptr, FALSE, L"pseudo_camera_thread_mutex");
	m_cam_thread = ::CreateThread(nullptr, 0x1000L, CameraFrameThread, this, 0x0, &m_cam_thread_id);
	return S_OK;
}

void CPseudoCamera::FinalRelease()
{
	if (m_cam_thread_mutex)
	{
		::CloseHandle(m_cam_thread_mutex);
		m_cam_thread_mutex = nullptr;
	}

	::CloseHandle(m_cam_thread);
}

void Output(const char* szFormat,...)
{
	char szBuff[1024];
	va_list arg;
	va_start(arg, szFormat);
	_vsnprintf_s(szBuff, sizeof(szBuff), szFormat, arg);
	va_end(arg);
	OutputDebugStringA(szBuff);
}


// EDIT - exposing as an incoming interface to simplify cross-apartment calls
// IPseudoCameraEvents

STDMETHODIMP CPseudoCamera::NewSpliceImage(FasCamBITMAPINFO *bmp_info)
{
	return Fire_NewSpliceImage(bmp_info);
}



DWORD WINAPI CPseudoCamera::CameraFrameThread(LPVOID context)
{
	HRESULT hr;
	static unsigned errornum = 0;
	unsigned __int32 cam_result, nu, mu;
	auto obj = static_cast<CPseudoCamera *>(context);
	if (obj == nullptr) return 0;
	auto orientation = static_cast<FasCamOrientationType>(-1);	// CameraFrameThread is created before orientation is read from XML

	::CoInitializeEx(nullptr, FASCAM_THREAD_MODEL);				    // All threads must be CoInitialize'd to be COM compatible.

	// EDIT ----------------------------
	_IPseudoCameraEvents *pPseudoCam = NULL;
	if (obj->m_pStream)
	{
		::CoGetInterfaceAndReleaseStream(obj->m_pStream, __uuidof(_IPseudoCameraEvents), (void **)&pPseudoCam);
		// This automatically releases obj->m_pStream.
	}
	// EDIT end -------------------------

	// Initialize the FasCamBITMAPINFO instance.
	auto x = 1024;
	auto y = 768;
	auto bytesperpixel = 1;
	auto yumpp = 1;
	auto zumpp = 1;

	long pad = ((x * bytesperpixel) % sizeof(COLORREF)) ? sizeof(COLORREF) - ((x * bytesperpixel) % sizeof(COLORREF)) : 0;
	long mem = sizeof(FasCamBITMAPINFO) + (bytesperpixel * (x + pad) * ::labs(y)); // Total number of bytes to alloc
	auto pCapturedImage = static_cast<FasCamBITMAPINFO *>(::CoTaskMemAlloc(mem));			    // Allocate on the global heap.
	if (pCapturedImage == nullptr) return false;                             // Check for allocation failure.

	pCapturedImage->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pCapturedImage->bmiHeader.biPlanes = 1;
	pCapturedImage->bmiHeader.biCompression = BI_RGB;
	pCapturedImage->bmiHeader.biClrImportant = 0;
	pCapturedImage->bmiHeader.biXPelsPerMeter = 0x8000;                      // TODO: Apect ratio of 1.0?
	pCapturedImage->bmiHeader.biYPelsPerMeter = 0x8000;                      // TODO: Apect ratio of 1.0?
	pCapturedImage->bmiHeader.biWidth = x;
	pCapturedImage->bmiHeader.biHeight = y;
	pCapturedImage->bmiHeader.biBitCount = bytesperpixel * 8;
	pCapturedImage->bmiHeader.biClrUsed = (bytesperpixel > 1/*8*/) ? 0 : IMAGE_PALETTE_SIZE; // There's no palette for 16,24 & 32-bit bitmaps.
	pCapturedImage->bmiHeader.biSizeImage = bytesperpixel * (x + pad) * ::labs(y); // Bitmap rows must be word-aligned, so pad out if necessary.

	pCapturedImage->bmiYMicronsPerPixel = yumpp;                             // y-dim scaling coefficient.
	pCapturedImage->bmiZMicronsPerPixel = zumpp;                             // z-dim scaling coefficient.
	pCapturedImage->bmiCoSize = pCapturedImage->bmiHeader.biSizeImage;		// EDIT

	::GetSystemTimeAsFileTime(reinterpret_cast<LPFILETIME>(&pCapturedImage->bmiTimeStamp));

	// Construct the monochrome 256 grey-scale palette...
	for (auto i_pal = 0; i_pal < IMAGE_PALETTE_SIZE; i_pal++)
	{
		pCapturedImage->bmiColors[i_pal].rgbRed = static_cast<BYTE>(i_pal);
		pCapturedImage->bmiColors[i_pal].rgbGreen = static_cast<BYTE>(i_pal);
		pCapturedImage->bmiColors[i_pal].rgbBlue = static_cast<BYTE>(i_pal);
		pCapturedImage->bmiColors[i_pal].rgbReserved = 0;
	}

	// Now set up the bitfield buffer pointer...
	pCapturedImage->bmiBits = reinterpret_cast<LPBYTE>(pCapturedImage) + sizeof(FasCamBITMAPINFO);
	::SecureZeroMemory(pCapturedImage->bmiBits, pCapturedImage->bmiHeader.biSizeImage);
	unsigned char grey = 0;

	do
	{
		::Sleep(250);  //Post one image every 250ms
		grey += 0x10;
		//different grey scale every time.
		const auto _currentValue = grey;

		//for (auto j = 0; j < x*y; j++)
		//{
		//	*(pCapturedImage->bmiBits + j) = _currentValue;
		//}

		memset(pCapturedImage->bmiBits, _currentValue, x*y);

		//obj->Fire_NewSpliceImage(pCapturedImage);						// EDIT
		if (pPseudoCam) pPseudoCam->NewSpliceImage(pCapturedImage);		// EDIT

		Output("_currentValue = %02X\n", _currentValue);
		
	} while (true);

	if (pPseudoCam) pPseudoCam->Release();	// EDIT

	return 0;
}
