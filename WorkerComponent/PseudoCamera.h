// PseudoCamera.h : Declaration of the CPseudoCamera

#pragma once
#include "resource.h"       // main symbols



#include "WorkerComponent.h"
#include "_IPseudoCameraEvents_CP.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CPseudoCamera

class ATL_NO_VTABLE CPseudoCamera :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CPseudoCamera, &CLSID_PseudoCamera>,
	public IConnectionPointContainerImpl<CPseudoCamera>,
	public CProxy_IPseudoCameraEvents<CPseudoCamera>,
	public _IPseudoCameraEvents,	// EDIT
	public IDispatchImpl<IPseudoCamera, &IID_IPseudoCamera, &LIBID_WorkerComponentLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
private:
	HANDLE m_cam_thread;                                    // Frame aquisition thread handle.
	ULONG m_cam_thread_id;                                  // ID of frame aquisition thread.
	HANDLE m_cam_thread_mutex;						        // Synch object for the aquisition system.

protected:
	static DWORD WINAPI CameraFrameThread(LPVOID context);    // Image aquisition worker thread.


public:
	CPseudoCamera()
	{
	}

DECLARE_REGISTRY_RESOURCEID(106)


BEGIN_COM_MAP(CPseudoCamera)
	COM_INTERFACE_ENTRY(IPseudoCamera)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(_IPseudoCameraEvents)			// EDIT
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CPseudoCamera)
	CONNECTION_POINT_ENTRY(__uuidof(_IPseudoCameraEvents))
END_CONNECTION_POINT_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();

	void FinalRelease();

public:

	//_IPseudoCameraEvents									// EDIT
	STDMETHOD(NewSpliceImage)(FasCamBITMAPINFO *bmp_info);	// EDIT
	IStream *m_pStream;										// EDIT
};

OBJECT_ENTRY_AUTO(__uuidof(PseudoCamera), CPseudoCamera)
