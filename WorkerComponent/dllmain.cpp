// dllmain.cpp : Implementation of DllMain.

#include "stdafx.h"
#include "resource.h"
#include "WorkerComponent.h"
#include "dllmain.h"
#include "compreg.h"
#include "xdlldata.h"

CWorkerComponentModule _AtlModule;

class CWorkerComponentApp : public CWinApp
{
public:

// Overrides
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CWorkerComponentApp, CWinApp)
END_MESSAGE_MAP()

CWorkerComponentApp theApp;

BOOL CWorkerComponentApp::InitInstance()
{
#ifdef _MERGE_PROXYSTUB
	if (!PrxDllMain(m_hInstance, DLL_PROCESS_ATTACH, nullptr))
		return FALSE;
#endif
	return CWinApp::InitInstance();
}

int CWorkerComponentApp::ExitInstance()
{
	return CWinApp::ExitInstance();
}
